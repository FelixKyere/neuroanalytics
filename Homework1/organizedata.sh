#!/bin/bash

#Organizing data based on filenames

#fileseries= /nas/longleaf/home/felixk/fileseries
mkdir fakedata

for i in `seq 1 50`; 

do
    echo "moving donor${i}/*.txt"; #let user know which files are being moved
    mkdir fakedata/donor${i} #make directory with donor number
    mv donor${i}_tp*.txt fakedata/donor${i} #move files with similar donor number and containg .txt into donor directory


done
