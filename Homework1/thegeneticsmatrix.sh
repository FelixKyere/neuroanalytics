#!/bin/bash

#script to create the matrix that prints line by line

VAR1=$(wc -l hapmap1.ped); #creating variable 1 that counts all lines
echo $VAR1; #print content of variable 1
VAR2=$(basename $VAR1); #creating variable 2 that only keeps the number of lines and not strings
echo $VAR2; #print content of variable 2

for i in `seq 1 $VAR2` #iterating through printing lines 1-89
do 
head -$i hapmap1.ped | tail -1
sleep 1
done



