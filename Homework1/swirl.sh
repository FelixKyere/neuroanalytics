#!/bin/bash

#FIRST PART,UNEDITED EXCEPT FOR COMMENTS

echo -ne '/'; #-n=prevents printing the previous command; -e= enables interpretation of backslash escapes; prints forward slash
sleep 1;#enables delay in execution of the next command by 1s
echo -ne '\b-';#-n=prevents printing the previous command; -e=enables interpreatation of backslash escapes; \b=backspace(deletes back by one character); -=prints - character
sleep 1;#enables delay in execution of the next command by 1s
echo -ne '\b\\';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; \\=prints the backslash
sleep 1;#enables delay in execution of the next command by 1s
echo -ne '\b|';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; |=prints |
sleep 1;#enables delay in execution of the next command by 1s
echo -ne '\b/';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; /=prints /
sleep 1;#enables delay in execution of the next command by 1s
echo -ne '\b-';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; -=prints -
sleep 1;#enables delay in execution of the next command by 1s
echo -ne '\b\\';#-n=prevents printing the previous command; -e =enables interpretation of backslash escapes; \b=backspace; //=prints backslash
sleep 1;#enables delay in execution of the next command by 1s
echo -ne '\b|';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; |=prints |


#SECOND PART WITH ADDITIONS

#spinning opposite direction
sleep 1;#delay execution for 1s
echo -ne "\b\\"; #\b=backspace; \\=prints backslash
sleep 1; #delay execution for 1s
echo -ne '\b-';#\b=backspace; -=prints -
sleep 1; #delay execution for 1s
echo -ne '\b/'; #/b=backslash; /=prints /
sleep 1; #delay execution for 1s
echo -ne '\b|';#\b=backspace; |=prints |
sleep 1; #delay execution for 1s
echo -ne '\b\\'; #\b=backspace; \\=prints backslash
sleep 1; #delay execution for 1s
echo -ne '\b-';#\b=backspace; -prints -
sleep 1; #delay execution for 1s
echo -ne '\b/';#\backspace; /=prints /
sleep 1; #delay execution for 1s
echo -ne '\b|'; #/b=backspace; |=prints |

#Spinning faster
sleep 0.4;#delay execution for 1s
echo -ne "\b\\"; #\b=backspace; \\=prints backslash
sleep 0.4; #delay execution for 1s
echo -ne '\b-';#\b=backspace; -=prints -
sleep 0.4; #delay execution for 1s
echo -ne '\b/'; #/b=backslash; /=prints /
sleep 0.4; #delay execution for 1s
echo -ne '\b|';#\b=backspace; |=prints |
sleep 0.4; #delay execution for 1s
echo -ne '\b\\'; #\b=backspace; \\=prints backslash
sleep 0.4; #delay execution for 1s
echo -ne '\b-';#\b=backspace; -prints -
sleep 0.4; #delay execution for 1s
echo -ne '\b/';#\backspace; /=prints /
sleep 0.4; #delay execution for 1s
echo -ne '\b|'; #/b=backspace; |=prints |
sleep 0.4; #delay execution for 1s
echo -ne '\b/'; #-n=prevents printing the previous command; -e= enables interpretation of backslash escapes; prints forward slash
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b-';#-n=prevents printing the previous command; -e=enables interpreatation of backslash escapes; \b=backspace(deletes back by one character); -=prints - character
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b\\';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; \\=prints the backslash
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b|';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; |=prints |
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b/';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; /=prints /
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b-';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; -=prints -
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b\\';#-n=prevents printing the previous command; -e =enables interpretation of backslash escapes; \b=backspace; //=prints backslash
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b|';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; |=prints |
sleep 0.4;#delay execution for 1s
echo -ne "\b"; #\b=backspace; 


#For loops for 10 revolutions

for i in `seq 1 10`
do
    echo "Executing Loop$i"
echo -ne "\b\\"; #\b=backspace; \\=prints backslash
sleep 0.4; #delay execution for 1s
echo -ne '\b-';#\b=backspace; -=prints -
sleep 0.4; #delay execution for 1s
echo -ne '\b/'; #/b=backslash; /=prints /
sleep 0.4; #delay execution for 1s
echo -ne '\b|';#\b=backspace; |=prints |
sleep 0.4; #delay execution for 1s
echo -ne '\b\\'; #\b=backspace; \\=prints backslash
sleep 0.4; #delay execution for 1s
echo -ne '\b-';#\b=backspace; -prints -
sleep 0.4; #delay execution for 1s
echo -ne '\b/';#\backspace; /=prints /
sleep 0.4; #delay execution for 1s
echo -ne '\b|'; #/b=backspace; |=prints |
sleep 0.4; #delay execution for 1s
echo -ne '\b/'; #-n=prevents printing the previous command; -e= enables interpretation of backslash escapes; prints forward slash
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b-';#-n=prevents printing the previous command; -e=enables interpreatation of backslash escapes; \b=backspace(deletes back by one character); -=prints - character
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b\\';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; \\=prints the backslash
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b|';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; |=prints |
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b/';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; /=prints /
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b-';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; -=prints -
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b\\';#-n=prevents printing the previous command; -e =enables interpretation of backslash escapes; \b=backspace; //=prints backslash
sleep 0.4;#enables delay in execution of the next command by 1s
echo -ne '\b|';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; |=prints |
echo -ne '\b'; #-n=prevents printing the previous command; -e= enables interpretation of backslash escapes

done



#Being creative with for loops
for i in `seq 1 10`
do
    echo "Being creative with Loop$i"
echo -ne '\b/'; #-n=prevents printing the previous command; -e= enables interpretation of backslash escapes; prints forward slash
sleep 0.2;#enables delay in execution of the next command by 0.2s
echo -ne '\b\t';#horizontal tab
sleep 0.2;#enables delay in execution of the next command by 0.2s
echo 'horizontal tab executed';
sleep 0.2;#enables delay in execution of the next command by 0.2s
echo -ne '\b-';#-n=prevents printing the previous command; -e=enables interpreatation of backslash escapes; \b=backspace(deletes back by one character); -=prints - character
sleep 0.2;#enables delay in execution of the next command by 0.2s
echo -ne '\b\\';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; \\=prints the backslash
sleep 0.2;#enables delay in execution of the next command by 0.2s
echo -ne '\b|';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; |=prints |
sleep 0.2;#enables delay in execution of the next command by 0.2s
echo -ne '\b\\'; #\b=backspace; \\=prints backslash
sleep 1; #delay execution for 1s
echo -ne '\b-';#\b=backspace; -prints -
sleep 1; #delay execution for 1s
echo -ne '\b/';#\backspace; /=prints /
sleep 1; #delay execution for 1s
echo -ne '\b|'; #/b=backspace; |=prints |
sleep 0.7;#enables delay in execution of the next command by 0.7s
echo -ne '\b/';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; /=prints /
sleep 0.7;#enables delay in execution of the next command by 0.7s
echo -ne '\b-';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; -=prints -
sleep 0.7;#enables delay in execution of the next command by 0.7s
echo -ne '\b\\';#-n=prevents printing the previous command; -e =enables interpretation of backslash escapes; \b=backspace; //=prints backslash
sleep 0.7;#enables delay in execution of the next command by 0.7s
echo -ne '\b|';#-n=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; |=prints |
sleep 0.7;#delay execution for 0.7s
echo -ne "\b\\"; #\b=backspace; \\=prints backslash
sleep 0.7; #delay execution for 0.7s
echo -ne '\b-';#\b=backspace; -=prints -
sleep 0.7; #delay execution for 0.7s
echo -ne '\b/'; #/b=backspace; /=prints /
sleep 0.7; #delay execution for 0.7s
echo -ne '\b'; #/b=backspace
done

echo -ne "\b \n";#-=prevents printing the previous command; -e=enables interpretation of backslash escapes; \b=backspace; \n=escapes to a new line
