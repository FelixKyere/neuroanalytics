options(stringsAsFactors=FALSE);
library(tidyverse)

setwd('/nas/longleaf/home/felixk') # set working directory

#Do some QC to remove high missingness SNPs

#Make a plot of genotype x phenotype for the first 100 SNPs

#Specify filenames of interest
fped  <- "hapmap1.ped"; 
fmap <- "hapmap1.map"; 
fphe <- "qt.phe";      

fsnp <- 'highmissingsnps.txt'
fpedout <- 'hapmap1_nomissing.ped'
fmapout <- 'hapmap1_nomissing.map'

flowafsnps <- 'lowafsnps.txt';
flowafmap <- 'hapmap1_nomissing_AF.map';
flowafped <- 'hapmap1_nomissing_AF.ped';
fpdf <- 'phenoxgeno.pdf';




#Munge my ped file
ped <- read.table(fped); #read fped

temp <- ped[1:20, 1:20] #.ped is too big so do this to view a subset of ped

pedsub <- ped[ ,(7:ncol(ped))] #subset and remove first six columns

donor_number <- nrow(pedsub) #assigning donor number to a variable

number_of_SNPs <- ((ncol(pedsub)/2)) #checking for number of SNPs

#output to user
cat('The total number of SNPs is: ',number_of_SNPs,'\n');
cat('The total number of donors is: ',donor_number,'\n');

#Reformatting the ped file to have
#0 0 = NA
#1 1 = homozygous for alle 1 = 0
#1 2 = heterozygous = 1
#2 2 = homozygous for allele 2 =2 

SNPs <- matrix(NA,nrow=donor_number,ncol=number_of_SNPs); #build an empty matrix with the same data structure. Helps runs faster than c()

#Loop over each pai of columns in the pedsub matrix
for (i in 1:number_of_SNPs){
  testSNPs <- matrix(NA,nrow=donor_number,ncol=1)
  test1 <- pedsub[,c(i*2-1,i*2)]
  
  No_genotype_NA <- which(test1[,1]==0 & test1[,2]==0)
  Homozygous_1  <- which(test1[,1]==1 & test1[,2]==1)
  Homozygous_2  <- which(test1[,1]==2 & test1[,2]==2)
  Heterozygous  <- which((test1[,1]==1 & test1[,2]==2) |  (test1[,1]==2 & test1[,2]==1) )
  
  #Recoded SNPs
  testSNPs[Heterozygous] <- 1
  testSNPs[No_genotype_NA] <- NA
  testSNPs[Homozygous_1] <- 0
  testSNPs[Homozygous_2] <- 2
  
  SNPs[,i] <- testSNPs #adding to SNP matrix
}

#use the map file to label which SNP is being referred to in the SNPs file
map = read.table(fmap)

#check if the number of SNPs in map file equals the number of SNPs in the ped file
if(nrow(map)!=number_of_SNPs) {
  cat('The number of snps in the map file is not equal to the number of SNPs in the ped file','\n')
}

#Assign SNP names to the columns
colnames(SNPs) = map$V2

#Assign row names for each donor
rownames(SNPs) = ped[,1]

#Calculate the missingness rate for each donor
missingnessrate <- colSums(is.na(SNPs))/ donor_number

#Get the indices of SNPs with >5% missingness
missingind <- which(missingnessrate>0.05);

#Print out the number of missing SNPs
cat('Total number of high missingness SNPs is:',length(missingind),'\n')

#write out the rsIDs of all missing SNPs
missingrs <- colnames(SNPs)[missingind]
write.table(missingrs,file=fsnp,quote=FALSE,row.names=FALSE,col.names =FALSE)

#subset the SNPs to only those that survive missingness threshold
#do map file first because it is easier
map.noms <- map[-missingind,]
write.table(map.noms,file=fmapout,quote=FALSE,row.names=FALSE,col.names=FALSE)

#For ped file
SNPs.noms <- SNPs[,-missingind]
pedSNPs.noms <- matrix(NA,nrow=donor_number,ncol=(2*ncol(SNPs.noms)))
for (i in 1:ncol(SNPs.noms)) {
  NAind <- which(is.na(SNPs.noms[,i]))
  hom1ind <- which(SNPs.noms[,i]==0)
  hetind <- which(SNPs.noms[,i]==1)
  hom2ind <- which(SNPs.noms[,i]==2)
  
  newpedind1 <- i*2-1
  newpedind2 <- i*2
  
  pedSNPs.noms[NAind,newpedind1] <- 0
  pedSNPs.noms[NAind,newpedind2] <- 0
  pedSNPs.noms[hom1ind,newpedind1] <- 1
  pedSNPs.noms[hom1ind,newpedind2] <- 1
  pedSNPs.noms[hetind,newpedind1] <- 1
  pedSNPs.noms[hetind,newpedind2] <- 2
  pedSNPs.noms[hom2ind,newpedind1] <- 2
  pedSNPs.noms[hom2ind,newpedind2] <- 2
}

#Add on the first 6 columns to the ped file
ped.noms <- cbind(ped[,1:6],pedSNPs.noms)
#Write out the file
write.table(ped.noms,file=fpedout,quote=FALSE,row.names=FALSE,col.names=FALSE)

#Frequency filter
#Filter for >5% or 95% allele frequency
nonmissingdonors <- colSums(!is.na(SNPs.noms))
AF <- colSums(SNPs.noms,na.rm=TRUE)/(nonmissingdonors*2)
lowafind <- which(AF>0.95 | AF<0.05)

#Get the SNP names for the low AF SNPs
lowafsnps <- colnames(SNPs.noms)[lowafind]

#write these out to a file
write.table(lowafsnps,file=flowafsnps,quote=FALSE,row.names = FALSE,col.names=FALSE)

#Remove low MAF from the map file
maplowaf <- map.noms[-lowafind,]
write.table(maplowaf,file=flowafmap,quote=FALSE,row.names=FALSE,col.names=FALSE)

#Repeat for ped file
SNPs.lowmaf <- SNPs.noms[,-lowafind]
pedSNPs.lowmaf <- matrix(NA,nrow=donor_number,ncol=(2*ncol(SNPs.lowmaf)))
for (i in 1:ncol(SNPs.lowmaf)){
  NAind <- which(is.na(SNPs.lowmaf[,i]))
  hom1ind <- which(SNPs.lowmaf[,i]==0)
  hetind <- which(SNPs.lowmaf[,i]==1)
  hom2ind <- which(SNPs.lowmaf[,i]==2)
  
  newpedind1 <- i*2-1
  newpedind2 <- i*2
  
  pedSNPs.lowmaf[NAind,newpedind1] <- 0
  pedSNPs.lowmaf[NAind,newpedind2] <- 0
  pedSNPs.lowmaf[hom1ind,newpedind1] <- 1
  pedSNPs.lowmaf[hom1ind,newpedind2] <- 1
  pedSNPs.lowmaf[hetind,newpedind1] <- 1
  pedSNPs.lowmaf[hetind,newpedind2] <- 2
  pedSNPs.lowmaf[hom2ind,newpedind1] <- 2
  pedSNPs.lowmaf[hom2ind,newpedind2] <- 2
  
}

#Add on the first 6 columns to the ped file
ped.lowmaf <- cbind(ped[,1:6],pedSNPs.lowmaf)

#write out the file
write.table(ped.lowmaf,file=flowafped,quote=FALSE,row.names = FALSE,col.names=FALSE)












