options(stringsAsFactors=FALSE);

#Make a plot of genotype x phenotype for the first 100 SNPs

#Specify filenames 
fped  <- "hapmap1.ped"; 
fmap <- "hapmap1.map"; 
fphe <- "qt.phe";      

setwd('/nas/longleaf/home/felixk') # set working directory

#Munge my ped file
ped <- read.table(fped); #read fped

temp <- ped[1:20, 1:20] #.ped is too big so do this to view a subset of ped

pedsub <- ped[ ,(7:ncol(ped))] #subset and remove first six columns

donor_number <- nrow(pedsub) #assigning donor number to a variable

number_of_SNPs <- ((ncol(pedsub)/2)) #checking for number of SNPs

#Reformatting genotype data with for loop
SNPs <- matrix(NA,nrow=donor_number,ncol=number_of_SNPs); #build an empty matrix with the same data structure. Helps runs faster than c()

for (i in 1:number_of_SNPs){
  testSNPs <- matrix(NA,nrow=donor_number,ncol=1)
  test1 <- pedsub[,c(i*2-1,i*2)]
  
  No_genotype_NA <- which(test1[,1]==0 & test1[,2]==0)
  Homozygous_1  <- which(test1[,1]==1 & test1[,2]==1)
  Homozygous_2  <- which(test1[,1]==2 & test1[,2]==2)
  Heterozygous  <- which((test1[,1]==1 & test1[,2]==2) |  (test1[,1]==2 & test1[,2]==1) )
  
  #Recoded SNPs
  testSNPs[Heterozygous] <- 1
  testSNPs[No_genotype_NA] <- NA
  testSNPs[Homozygous_1] <- 0
  testSNPs[Homozygous_2] <- 2
  
  SNPs[,i] <- testSNPs #adding to SNP matrix
}

#Assign row names for each donor
rownames(SNPs) = ped[,1]


#Read in map file to get SNPid
map = read.table(fmap)
colnames(SNPs) = map$V2

#Read in phenotype file
phe = read.table(fphe)
pheno = phe$V3

#Open a pdf file to store the output in
pdf("genotypeassociation.pdf");

for (z in 1:100){
  
  plot(SNPs[,z],pheno,main=colnames(SNPs)[z],xlab="Genotype",ylab="Phenotype");
  
}

#Close file to store the graphs
dev.off();