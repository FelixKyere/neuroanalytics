###Script for data munging and analysis of MVX cortical length measurements for Chd8 project
###04/14/2022
###Goal 1: Testing for two outcome variables as an average measurement from 4 undergraduate raters taken from 2D mouse brains.
###Goal 2: Testing effects of predictors on outcome variable via linear regression
### outcome variable 1:Total cortical length of the left and right cortical hemispheres.
### outcome variable 2: Laterality index calculated as (left hemisphere cortical length-right hemisphere cortical length)/(left hemisphere cortical length+right hemisphere cortical length)
###Undergraduate raters: C=Carla; Me=Meghana; R=Rohan; Ma=Maryam


##Setting up environment
options(stringsAsFactors=FALSE);
library(tidyverse)
library(ggplot2)
#installing package needed for running stat_compare_means
install.packages("ggpubr")
library(ggpubr)
install.packages("dplyr")
library(nlme)


##reading dataframe
dat <- read.csv('/Users/spiracles182/Desktop/Documents/Documents - Felix’s MacBook Pro/UNC/Adobe Illustrator/MVX/Master /MVX_Master_spreadsheet.csv')
View(dat)
dat_average = dat[-nrow(dat),] #removes extra row created after generating csv file
View(dat_average)
#adding 2 new columns of average cortical length from all raters and average LI from all raters
dat_average <- mutate(dat_average,total_raters_cortical_length=(C_totalcorticallength + Me_totalcorticallength + R_totalcorticallength + Ma_totalcorticallength)/4, total_raters_LI=(C_LI + Me_LI + R_LI + Ma_LI)/4)
View(dat_average)

##Plots

#Plotting Total Cortical Length
dat_average$Genotype = relevel(as.factor(dat_average$Genotype), ref = "WT")
print(ggplot(data = dat_average, aes(x = Genotype, y =total_raters_cortical_length,color=Sex )) +
              geom_line(aes(group=ID2), size=1.0) +
              geom_point(size=7, alpha=1) +
              ggtitle(paste0("Total Cortical Length")) + 
              stat_compare_means(label="p.format",method = "t.test", paired= TRUE) +
              ylab("cortical length(mm)") +
              xlab(paste0("")))

#Plotting Total LI
print(ggplot(data = dat_average, aes(x = Genotype, y =total_raters_LI,color=Sex )) +
              geom_line(aes(group=ID2), size=1.5) +
              geom_point(size=8, alpha=1) +
              ggtitle(paste0("Laterality Index")) + 
              stat_compare_means(label="p.format",method = "t.test", paired= TRUE) +
              ylab("Laterality index(LI)") +
              xlab(paste0("")))



##Testing Linear Regression one predictor at a time for Total Cortical Length

str(dat_average) #check the type of data for each column variable and treat as such whether character string or integer

#1. Genotype Linear Model
Genotype_model<-lm(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype))
summary(Genotype_model)
summary(Genotype_model)$coefficient
pval = summary(Genotype_model)$coefficient[2,4]
pval
boxplot(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype))

#2. Sex Linear Model
Sex_model<-lm(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Sex))
summary(Sex_model)
summary(Sex_model)$coefficient
pval = summary(Sex_model)$coefficient[2,4]
pval
boxplot(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Sex))

#3. Litter Size Linear Model
Littersize_model<-lm(dat_average$total_raters_cortical_length ~ dat_average$Litter_size)
summary(Littersize_model)
summary(Littersize_model)$coefficient
plot(dat_average$Litter_size,dat_average$total_raters_cortical_length) 

#4.Time of Dissection Linear Model
new_dat_average = dat_average[which(dat_average$Time_of_Dissection != "null"),] #filter out null values
View(new_dat_average)
Timeofdissection_model<-lm(new_dat_average$total_raters_cortical_length ~ new_dat_average$Time_of_Dissection)
summary(Timeofdissection_model)
summary(Timeofdissection_model)$coefficient
plot(new_dat_average$Time_of_Dissection,new_dat_average$total_raters_cortical_length) 

#5.Perfusion Linear Model
new_dat_average = dat_average[which(dat_average$Perfusion_prohance != "null"),] #filter out null values
View(new_dat_average)
Perfusion_model<-lm(new_dat_average$total_raters_cortical_length ~ as.factor(new_dat_average$Perfusion_prohance))
summary(Perfusion_model)
summary(Perfusion_model)$coefficient
plot(as.factor(new_dat_average$Perfusion_prohance),new_dat_average$total_raters_cortical_length) 

#6.Order of Dissection Linear Model
Order_of_Dissection_model<-lm(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Order_of_Dissection))
summary(Order_of_Dissection_model)
summary(Order_of_Dissection_model)$coefficient
plot(as.factor(dat_average$Order_of_Dissection),dat_average$total_raters_cortical_length) 

#7.Age of Dam Linear Model(Positive correlation and significant)
Age_of_Dam_model<-lm(dat_average$total_raters_cortical_length ~ dat_average$Age_of_Dam)
summary(Age_of_Dam_model)
summary(Age_of_Dam_model)$coefficient
plot(dat_average$Age_of_Dam,dat_average$total_raters_cortical_length)

#8.Age of sire Linear Model
Age_of_sire_model<-lm(dat_average$total_raters_cortical_length ~ dat_average$Age_of_sire)
summary(Age_of_sire_model)
summary(Age_of_sire_model)$coefficient
plot(dat_average$Age_of_sire,dat_average$total_raters_cortical_length) 

#9.Sire last litter Linear Model
new_dat_average = dat_average[which(dat_average$Sire_last_litter != "null"),] #filter out null values
View(new_dat_average)
Sire_last_litter_model<-lm(new_dat_average$total_raters_cortical_length ~ as.factor(new_dat_average$Sire_last_litter))
summary(Sire_last_litter_model)
summary(Sire_last_litter_model)$coefficient
plot(as.factor(new_dat_average$Sire_last_litter),new_dat_average$total_raters_cortical_length) 

#10.Dam last litter Linear Model. Has only 2 data points after removal of null values. Not trustworthy
new_dat_average = dat_average[which(dat_average$Dam_last_litter != "null"),] #filter out null values
View(new_dat_average)
Dam_last_litter_model<-lm(new_dat_average$total_raters_cortical_length ~ as.factor(new_dat_average$Dam_last_litter))
summary(Dam_last_litter_model)
summary(Dam_last_litter_model)$coefficient
plot(as.factor(new_dat_average$Dam_last_litter),new_dat_average$total_raters_cortical_length) 




##Testing Linear Regression one predictor at a time for Laterality Index

str(dat_average) #check the type of data for each column variable and treat as such whether character string or integer

#1. Genotype Linear Model
Genotype_model<-lm(dat_average$total_raters_LI ~ as.factor(dat_average$Genotype))
summary(Genotype_model)
summary(Genotype_model)$coefficient
pval = summary(Genotype_model)$coefficient[2,4]
pval
boxplot(dat_average$total_raters_LI ~ as.factor(dat_average$Genotype))


#2. Sex Linear Model
Sex_model<-lm(dat_average$total_raters_LI ~ as.factor(dat_average$Sex))
summary(Sex_model)
summary(Sex_model)$coefficient
pval = summary(Sex_model)$coefficient[2,4]
pval
boxplot(dat_average$total_raters_LI ~ as.factor(dat_average$Sex))

#3. Litter Size Linear Model
Littersize_model<-lm(dat_average$total_raters_LI ~ dat_average$Litter_size)
summary(Littersize_model)
summary(Littersize_model)$coefficient
plot(dat_average$Litter_size,dat_average$total_raters_LI) 

#4.Time of Dissection Linear Model
new_dat_average = dat_average[which(dat_average$Time_of_Dissection != "null"),] #filter out null values
View(new_dat_average)
Timeofdissection_model<-lm(new_dat_average$total_raters_LI ~ new_dat_average$Time_of_Dissection)
summary(Timeofdissection_model)
summary(Timeofdissection_model)$coefficient
plot(new_dat_average$Time_of_Dissection,new_dat_average$total_raters_LI) 

#5.Perfusion Linear Model
new_dat_average = dat_average[which(dat_average$Perfusion_prohance != "null"),] #filter out null values
View(new_dat_average)
Perfusion_model<-lm(new_dat_average$total_raters_LI ~ as.factor(new_dat_average$Perfusion_prohance))
summary(Perfusion_model)
summary(Perfusion_model)$coefficient
plot(as.factor(new_dat_average$Perfusion_prohance),new_dat_average$total_raters_LI) 

#6.Order of Dissection Linear Model
Order_of_Dissection_model<-lm(dat_average$total_raters_LI ~ as.factor(dat_average$Order_of_Dissection))
summary(Order_of_Dissection_model)
summary(Order_of_Dissection_model)$coefficient
plot(as.factor(dat_average$Order_of_Dissection),dat_average$total_raters_LI) 

#7.Age of Dam Linear Model
Age_of_Dam_model<-lm(dat_average$total_raters_LI ~ dat_average$Age_of_Dam)
summary(Age_of_Dam_model)
summary(Age_of_Dam_model)$coefficient
plot(dat_average$Age_of_Dam,dat_average$total_raters_LI)

#8.Age of sire Linear Model
Age_of_sire_model<-lm(dat_average$total_raters_LI ~ dat_average$Age_of_sire)
summary(Age_of_sire_model)
summary(Age_of_sire_model)$coefficient
plot(dat_average$Age_of_sire,dat_average$total_raters_LI) 

#9.Sire last litter Linear Model
new_dat_average = dat_average[which(dat_average$Sire_last_litter != "null"),] #filter out null values
View(new_dat_average)
Sire_last_litter_model<-lm(new_dat_average$total_raters_LI ~ as.factor(new_dat_average$Sire_last_litter))
summary(Sire_last_litter_model)
summary(Sire_last_litter_model)$coefficient
plot(as.factor(new_dat_average$Sire_last_litter),new_dat_average$total_raters_LI) 

#10.Dam last litter Linear Model. Has only 2 data points after removal of null values. Not trustworthy
new_dat_average = dat_average[which(dat_average$Dam_last_litter != "null"),] #filter out null values
View(new_dat_average)
Dam_last_litter_model<-lm(new_dat_average$total_raters_LI ~ as.factor(new_dat_average$Dam_last_litter))
summary(Dam_last_litter_model)
summary(Dam_last_litter_model)$coefficient
plot(as.factor(new_dat_average$Dam_last_litter),new_dat_average$total_raters_LI) 


###Linear Mixed Model

#Assessing Genotype and Age of Dam on Cortical length. Non significant
Genotype_and_Age_of_Dam_model<-lm(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype)+dat_average$Age_of_Dam)
summary(Genotype_and_Age_of_Dam_model)
summary(Genotype_and_Age_of_Dam_model)$coefficient
pval = summary(Genotype_and_Age_of_Dam_model)$coefficient[2,4]
pval


#Assessing Genotype and Sex on Cortical length
Genotype_and_Sex_model<-lm(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype)+as.factor(dat_average$Sex))
summary(Genotype_and_Sex_model)
summary(Genotype_and_Sex_model)$coefficient
pval = summary(Genotype_and_Sex_model)$coefficient[2,4]
pval

#Assessing Genotype, Age of Dam and Sex on Cortical length
Genotype_Age_of_Dam_and_Sex_model<-lm(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype)+as.numeric(dat_average$Age_of_Dam)+as.factor(dat_average$Sex))
summary(Genotype_Age_of_Dam_and_Sex_model)
summary(Genotype_Age_of_Dam_and_Sex_model)$coefficient
pval = summary(Genotype_Age_of_Dam_and_Sex_model)$coefficient[2,4]
pval
summary(Genotype_Age_of_Dam_and_Sex_model)

#Interaction model genotype+sex:genotype+sex
Genotype_and_Sex_model<-lm(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype)+as.factor(dat_average$Sex):as.factor(dat_average$Genotype)+as.factor(dat_average$Sex))
summary(Genotype_and_Sex_model)
summary(Genotype_and_Sex_model)$coefficient
pval = summary(Genotype_and_Sex_model)$coefficient[2,4]
pval
summary(Genotype_and_Sex_model)

#Interaction model genotype+age of dam:genotype+age of dam
Genotype_and_Age_of_Dam<-lm(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype)+as.numeric(dat_average$Age_of_Dam):as.factor(dat_average$Genotype)+as.numeric(dat_average$Age_of_Dam))
summary(Genotype_and_Age_of_Dam)
summary(Genotype_and_Age_of_Dam)$coefficient
pval = summary(Genotype_and_Age_of_Dam)$coefficient[2,4]
pval
summary(Genotype_and_Age_of_Dam)

#Interaction model genotype + age of dam *age of dam
Genotype_and_Age_of_Dam<-lm(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype)+as.numeric(dat_average$Age_of_Dam) * as.numeric(dat_average$Age_of_Dam))
summary(Genotype_and_Age_of_Dam)
summary(Genotype_and_Age_of_Dam)$coefficient
pval = summary(Genotype_and_Age_of_Dam)$coefficient[2,4]
pval
summary(Genotype_and_Age_of_Dam)

#Interaction model genotype + age of dam * sex
Genotype_and_Age_of_Dam<-lm(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype)+as.numeric(dat_average$Age_of_Dam) * as.factor(dat_average$Sex))
summary(Genotype_and_Age_of_Dam)
summary(Genotype_and_Age_of_Dam)$coefficient
pval = summary(Genotype_and_Age_of_Dam)$coefficient[2,4]
pval
summary(Genotype_and_Age_of_Dam)

#Interaction model genotype + age of dam * age of dam * sex
Genotype_and_Age_of_Dam<-lm(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype)+as.numeric(dat_average$Age_of_Dam) * as.numeric(dat_average$Age_of_Dam) * as.factor(dat_average$Sex))
summary(Genotype_and_Age_of_Dam)
summary(Genotype_and_Age_of_Dam)$coefficient
pval = summary(Genotype_and_Age_of_Dam)$coefficient[2,4]
pval
summary(Genotype_and_Age_of_Dam)

###Non linear models
library(nlme)

##lme(length ~ genotype + sex, rand = ~1|litter)
Genotype_and_Sex_model<-lme(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype) + as.factor(dat_average$Sex), rand = ~1|dat_average$Litter_size)
summary(Genotype_and_Sex_model)
summary(Genotype_and_Sex_model)$coefficient
pval = summary(Genotype_and_Sex_model)$coefficient[2,4]

##lme(length ~ genotype + sex, rand = ~1|Age of Dam)
Genotype_and_Sex_model<-lme(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype) + as.factor(dat_average$Sex), rand = ~1|as.numeric(dat_average$Age_of_Dam))
summary(Genotype_and_Sex_model)
summary(Genotype_and_Sex_model)$coefficient
pval = summary(Genotype_and_Sex_model)$coefficient[2,4]

##lme(length ~ genotype, rand = ~1|Age of Dam)
Genotype_and_Sex_model<-lme(dat_average$total_raters_cortical_length ~ as.factor(dat_average$Genotype), rand = ~1|as.numeric(dat_average$Age_of_Dam))
summary(Genotype_and_Sex_model)
summary(Genotype_and_Sex_model)$coefficient
pval = summary(Genotype_and_Sex_model)$coefficient[2,4]


